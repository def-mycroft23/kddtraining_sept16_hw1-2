""" Solution to HW2 - two player sticks game.

Rules of the game:

    * The game board starts out with 20 sticks.
    * There are two players in each game.
    * During each turn, each player can take 1, 2 or 3 sticks.
    * The player who takes the last stick loses.

Please see README located in repository for further documentation.
"""
import os
import platform
import random


def clear_screen():
    """ Clears the screen."""
    os.system('cls' if platform.system() is 'Windows' else 'clear')


def initialize_board():
    """ Creates a new board and returns a list representing the board.

    Creates and returns a list of lists which represents a fresh board.

    For example, the first portion of a board looks like this:

        [['|', 1], ['|', 2], ['|', 3], ['|', 4],...,['|',20]

    Each stick is a "|" and each stick has an index number.
    """
    board_list = []

    for item in xrange(0, 9): # Sticks 1-9 have a leading zero.
        new_stick = []
        new_stick.append("|")
        new_stick.append("0%s" % str(item + 1)) # Append with zero.
        board_list.append(new_stick)

    for item in xrange(9, 20): # All other sticks simply have a number.
        new_stick = []
        new_stick.append("|")
        new_stick.append(str(item + 1))
        board_list.append(new_stick)

    return board_list


def display_board(current_board):
    """Displays the current board in the console.

    Assumes that there are 20 items in the board.

    Prints a board in this manner:

    ###########################################################
     |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |

    01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20

    ###########################################################
    """
    print "\n"
    print "#" * 59

    for item in xrange(0, 20): # Row of five "|"s.
        print "",
        print current_board[item][0],

    print "\n"
    for item in xrange(0, 20): # Row of number of first five "|"s.
        print current_board[item][1],

    print "\n"
    print "#" * 59
    print "\n"


def edit_board(current_board, player_choice):
    """ Removes a given number of sticks from the board.

    The current board and the choice of sticks are taken as an input.

    Iterates through the current board and removes the first x number
    of sticks from the board (the number of sticks corresponding to the
    choice of sticks given as an input.

    Returns the modified board.
    """
    i = 0
    for item in current_board:
        if item[0] == "|":
            item[0] = "x"
            i += 1 # Add 1 to i every time a stick is removed.
            if i == player_choice: # Remove only the correct quantity.
                break

    return current_board


def count_sticks(current_board):
    """ Given a board, returns the number of sticks in the board.

    Iterates through the list and if the item in the list is a stick,
    adds one to the count.
    """
    count = 0
    for item in current_board:
        if item[0] == "|":
            count += 1

    return count


def robot_take_turn(player_name, current_board):
    """ The robot takes a turn and chooses random possibilities.

    If the possibilities are greater than three, the bot chooses at
    random.

    If there are two or less sticks on the board, the bot chooses one.

    """
    clear_screen()
    print "And now the bot %s takes a turn..." % player_name

    stick_count = count_sticks(current_board)

    # Bot can take either 1, 2 or 3 sticks if there are three or more.
    # Bot only takes one if there are less than three.
    turn_sticks = random.randrange(1, 4) if stick_count >= 3 else 1

    current_board = edit_board(current_board, turn_sticks)
    print "New board:"
    display_board(current_board)

    turn_message = "The mighty bot %s took %s sticks" % (player_name,
                                                         turn_sticks)
    print turn_message
    print "Press enter to continue."
    raw_input("...")

    return current_board, turn_message


def take_turn(player_name, current_board):
    """ Allow the player to take a turn, return modified result.

    Displays the board and then asks the player to make a choice.

    The player is forced to make a legit choice depending on the number
    of sticks on the board.

    Returns the modified board and a record of the action taken.
    """
    clear_screen()
    print "Your turn %s !!" % player_name
    display_board(current_board)

    stick_count = count_sticks(current_board)

    if stick_count >= 3:
        # Player must take either 1, 2 or 3 sticks.

        print "%s, you can to take either 1, 2 or 3 sticks." % player_name
        turn_sticks = int(raw_input(" How many sticks? >>"))

        # Force the player to make a valid choice.
        while turn_sticks not in xrange(1, 4):
            print "Unfortunately, your choices are 1, 2 or 3."
            print "And your dilemma is that you must make a choice.."
            turn_sticks = int(raw_input(" How many sticks? >>"))

    if stick_count == 2:
        # If there are only two sticks, player must take one or both.
        # Player is free to choose to lose the game.

        print "%s, you can take either one or two sticks." % player_name
        turn_sticks = int(raw_input(" How many sticks? >>"))

        # Force the player to make a valid choice.
        while turn_sticks not in xrange(1, 3):
            print "You must take either one or two sticks..."
            turn_sticks = int(raw_input(" How many sticks? >>"))

    if stick_count == 1:
        # The player must take the last stick and lose.

        print "%s, your choice is pretty much 1 stick..." % player_name
        turn_sticks = int(raw_input(" How many sticks? >>"))

        # Force the player to make a valid choice.
        while turn_sticks != 1:
            print "Did you read the rules?"
            print "Allow me make this more clear: Take one stick."
            turn_sticks = int(raw_input(" Take one stick... >>"))

    # Edit the board depenting on the number of chosen sticks.
    current_board = edit_board(current_board, turn_sticks)
    print "New board:"
    display_board(current_board)

    # Print description of the turn.
    turn_message = "Player %s took %s sticks" % (player_name, turn_sticks)
    print turn_message

    return current_board, turn_message


def play_game():
    """ Walks players through the actual game and displays results.

    The loser is set to the last player to make any choice,
    and the player to pick the last stick has to make a choice.

    Maintains a list descriptions of the turn and returns this list at
    the end of the game.
    """
    clear_screen()
    print "Hello! Welcome to the game."
    print "Two players can play this game."

    print "The players must decide who goes first..."
    print "Please make this decision now."
    first_player = raw_input("First player name? >>")
    second_player = raw_input("Second player name? >>")
    current_board = initialize_board()

    stick_count = 20
    loser = first_player
    history = []

    # The stick count is after each turn, and loop terminates when zero.
    while stick_count != 0:

        # First player takes a turn.
        if stick_count != 0:

            current_board, new_history = take_turn(first_player, current_board)
            stick_count = count_sticks(current_board)
            history.append(new_history)
            loser = first_player

        # Second player takes a turn.
        if stick_count != 0:

            current_board, new_history = take_turn(second_player, current_board)
            stick_count = count_sticks(current_board)
            history.append(new_history)
            loser = second_player

    clear_screen()

    if loser == first_player:
        print "The winner is %s!!" % second_player
        print ""
        print "Here is the history of the game:"
        for item in history:
            print item

    if loser == second_player:
        print "The winner is %s!!" % first_player
        print "Here is the history of the game:"
        print ""
        for item in history:
            print item


def play_game_with_robot():
    """ Plays the game with a human and a robot.

    The loser is set to the last player to make any choice,
    and the player to pick the last stick has to make a choice.

    Maintains a list descriptions of the turn and returns this list at
    the end of the game.
    """
    clear_screen()
    print "Hello! Welcome to the game."
    print "You have chosen to play with a robot."

    robot_player = "The Terminator"
    print "Your opponent's name is %s." % robot_player
    human_player = raw_input("What is your name? >>")

    print "Since the robot has the advantage of being superhuman..."
    print "You will get to go first."
    print "(Superhuman at least with respect to speed of calculation)"
    raw_input("Press enter to continue...")

    current_board = initialize_board()

    stick_count = 20
    loser = human_player
    history = []

    # The stick count is after each turn, and loop terminates when zero.
    while stick_count != 0:

        # Human player takes turn.
        if stick_count != 0:

            current_board, new_history = take_turn(human_player, current_board)
            stick_count = count_sticks(current_board)
            history.append(new_history)
            loser = human_player

        # Robot player takes a turn.
        if stick_count != 0:

            current_board, new_history = robot_take_turn(robot_player,
                                                         current_board)
            stick_count = count_sticks(current_board)
            history.append(new_history)
            loser = robot_player

    clear_screen()

    if loser != human_player:
        print "The winner is %s!!" % human_player
        print ""
        print "Here is the history of the game:"
        for item in history:
            print item

    if loser == human_player:
        print "Sorry %s, you lost to the robot." % human_player
        print "Here is the history of the game:"
        print ""
        for item in history:
            print item


def menu():
    """ Allows the player to choose between a human or robot game."""

    print "Please choose either 1) Robot opponent or 2) Two players."
    ans = int(raw_input(" >>"))

    while ans not in xrange(1, 3):
        print "Please choose either 1) Robot opponent or 2) Two players."
        ans = raw_input(" >>")

    play_game() if ans == 2 else play_game_with_robot()

menu()
