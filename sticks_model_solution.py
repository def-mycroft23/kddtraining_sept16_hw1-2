import os

def get_sticks(message, total_sticks):
    sticks_taken = 0
    while sticks_taken < 1 or sticks_taken > 3:
        sticks_taken = int(input(message))

        if sticks_taken > total_sticks:
            print('You cannot take more sticks than are left')
            sticks_taken = 0

    return sticks_taken


def print_sticks(sticks):
    os.system('clear')
    for _ in range(5):
        print(' | ' * sticks)


def game():
    total_sticks = 20
    first_player_turn = False

    while total_sticks > 0:
        print_sticks(total_sticks)

        # turns
        first_player_turn = not first_player_turn
        player_name = 'First Player' if first_player_turn else 'Second Player'
        total_sticks -= get_sticks(player_name + ', make your move: ', total_sticks)

    print(player_name + ' you LOST!\n')


while True:
    game()

    if input('Play More? (y/n): ')[0].lower() != 'y':
        break
