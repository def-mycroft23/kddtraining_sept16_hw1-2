This one is marked as "done" for now. Later when I have time I may go back
into this one and try to cut down on the number of lines of code.

I have notes in this repo and a couple of TODO statements in the script. Also,
there is a model solution which can be used to compare.

##Solution to HW2 for KDD Lab - September 2016

Plays the game of sticks. User can choose to play with another human player,
or can choose a robot. Robot player basically makes random choices.

The "board" is represented as a list of lists, with each top-level list member
representing a stick and an index member.

Rules of the game:

* The game board starts out with 20 sticks.
* There are two players in each game.
* During each turn, each player can take 1, 2 or 3 sticks.
* The player who takes the last stick loses.

After each turn, a message is displayed indicating the player's choice. In the
board, a stick is represented by a "|" and each stick is replaced with an "x"
as a player takes it.

After the game, the winner is announced and the history of the game is
displayed.

##Short description of each function:

clear_screen() - 
Clears the screen

initialize_board() - 
Creates a new board and returns a list representing the board.

display_board(current_board) - 
Displays the current board in the console.

edit_board(current_board, player_choice) - 
Removes a given number of sticks from the board.

count_sticks(current_board) - 
Given a board, returns the number of sticks in the board.

take_turn(player_name, current_board) - 
Allow the player to take a turn, return modified board.

play_game() - 
Walks players through the actual game and displays results.

play_game_with_robot() - 
Plays the game with a human and a robot.

menu() - 
Allows the player to choose between either a human or robot game.

