
# Overview of Game
What does this game need to do: 

* Display the board.
* Update the board depending on the player choice.
* Prompt the player to take a choice.

# Displaying the Board 

I have a lot going on in this script, and something that I could do in order
to slim it down is in the way that the board is being displayed and kept track
of. I am creating a list for the board, when I could have just kept the board
as a number, and passed a number to a function that displays the board.

No list or anything... just create a function that prints out the board given
the number of sticks... in this case I could probably even work this into
the current display function - just have to pass in a variable instead of
assuming that the board has 20 sticks.

That solves a lot, because without having the list to deal with, can just
change a variable to update instead of needing to loop through a list.

I think that a little more planning would have helped here, instead of just
jumping into it and making up things as I go. This is probably a big reason
why this solution came out convoluted.
